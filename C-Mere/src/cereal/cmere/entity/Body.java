package cereal.cmere.entity;

import cereal.cmere.game.IDescribeable;
import cereal.cmere.game.phys.IMass;

public class Body implements IDescribeable, IMass {

	/**
	 * Height in m
	 */
	protected double height;
	/**
	 * Weight in kg
	 */
	protected double weight;
	protected float[] getFatDistribution = new float[] { 0.0f, 0.0f, 0.0f, 0.0f };
	/**
	 * Amount of flesh in m^3, or 1 kL
	 */
	protected double fleshVol;
	/**
	 * Amount of fat in m^3, or 1 kL
	 */
	protected double fatVol;
	/**
	 * Amount of muscle in m^3, or 1 kL
	 */
	protected double muscleVol;

	protected final String name;

	public Body(String name) {
		this.name = name;
	}

	@Deprecated
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		// TODO
		return null;
	}

	public void update() {

	}

	@Override
	public double getVolume() {
		return fleshVol + fatVol + muscleVol;
	}

	@Override
	public double getWeight() {
		return weight;
	}

}
