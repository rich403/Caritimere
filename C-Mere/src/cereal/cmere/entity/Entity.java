package cereal.cmere.entity;

import cereal.cmere.Game;
import cereal.cmere.Utils;
import cereal.cmere.game.IDFactory;
import cereal.cmere.game.IDescribeable;
import cereal.cmere.game.IInteractable;
import cereal.cmere.game.IMoveable;
import cereal.cmere.io.ISSerializable;
import cereal.cmere.map.ILocatable;
import cereal.cmere.map.MapPos;
import cereal.cmere.map.area.Area;
import net.keabotstudios.superserial.containers.SSObject;
import net.keabotstudios.superserial.containers.SSString;

public abstract class Entity implements IDescribeable, ILocatable, IMoveable, IInteractable, ISSerializable {

	protected String name;
	protected MapPos pos;

	public Entity(String name) {
		this.name = name;
		this.pos = MapPos.EMPTY();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return "It's " + Utils.article(getName()) + " " + getName() + ".";
	}

	@Override
	public MapPos getPos() {
		return pos;
	}

	public void update() {

	}

	@Override
	public boolean setPos(MapPos pos) {
		if (Game.instance().getCurrentMap().isPosWithin(pos)) {
			this.pos = pos;
			return true;
		}
		return false;
	}

	public Area getCurrentArea() {
		if (Game.instance().getCurrentMap() == null)
			return null;
		return Game.instance().getCurrentMap().getAreaAt(pos);
	}

	public float getMoveSpeedMultiplier() {
		return 1f;
	}

	@Override
	public String getSerialName() {
		return "entity:" + IDFactory.generateID();
	}

	public Class<? extends Entity> getEntityClass() {
		return this.getClass();
	}

	@Override
	public final SSObject serialize() {
		SSObject obj = new SSObject(getSerialName());
		obj.addString(new SSString("class", getEntityClass().getName()));
		obj.addString(new SSString("name", getName()));
		obj.addObject(getPos().serialize());
		serializeEntity(obj);
		return obj;
	}

	@Override
	public final void deserialize(SSObject data) {
		if (!data.getString("class").getString().equals(getEntityClass().getName()))
			throw new ClassFormatError("Entity class mismatch! \"" + data.getString("class") + "\" =/= \"" + getEntityClass().getName() + "\"");
		this.name = data.getString("name").getString();
		this.pos = MapPos.EMPTY();
		this.pos.deserialize(data.getObject("pos"));
		deserializeEntity(data);
	}

	public abstract void serializeEntity(SSObject obj);

	public abstract void deserializeEntity(SSObject data);
}
