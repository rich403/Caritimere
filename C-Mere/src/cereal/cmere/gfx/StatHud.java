package cereal.cmere.gfx;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics2D;

import cereal.cmere.Game;
import cereal.cmere.GameInfo;
import cereal.cmere.game.IStatistical;

public class StatHud {

	private long timer, hideTimer;
	private final long transitionLength = 200;
	private IStatistical tracked;

	private static final float maxAlpha = 0.8f;

	private static final byte CLOSED = 0x00;
	private static final byte OPENING = 0x02;
	private static final byte OPEN = 0x01;
	private static final byte CLOSING = 0x03;

	private byte status = CLOSED;

	private int width = 150, height = 30, x = GameInfo.WIDTH - 5 - width, y = 165;
	private byte barDir = ProgressBarRenderer.DIR_LEFT;

	public StatHud(IStatistical tracked) {
		this.tracked = tracked;
	}

	public void update() {
		switch (status) {
		case OPENING:
			if (Game.instance().getTimer() - timer >= transitionLength)
				status = OPEN;
			break;
		case CLOSING:
			if (Game.instance().getTimer() - timer >= transitionLength)
				status = CLOSED;
			break;
		default:
			timer = 0;
			break;
		}

		if (hideTimer > 0) {
			hideTimer--;
			if (hideTimer == 0) {
				hide();
			}
		}
	}

	public void draw(Graphics2D g) {
		g.setColor(Color.WHITE);
		TextRenderer.drawString(g, Game.instance().getClock().toString(), 5, 5);

		if (status == CLOSED)
			return;
		int tx = x;
		int ty = y;
		float alpha = maxAlpha;
		if (status != OPEN) {
			float openAmt = (float) (((double) Game.instance().getTimer() - (double) timer) / (double) transitionLength);
			if (status == OPENING) {
				tx = Math.round(x + ((x + 10f) * (1f - openAmt)));
				alpha = maxAlpha * (openAmt);
			} else {
				tx = Math.round(x + ((x + 10f) * (openAmt)));
				alpha = maxAlpha * (1f - openAmt);
			}
		}
		if (alpha <= 0f)
			return;
		alpha = Math.max(0f, Math.min(alpha, maxAlpha));
		Composite old = g.getComposite();
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
		g.translate(tx, ty);
		int cx = 0;
		int cy = 0;
		int bh = (int) Math.round(height / 3.0);
		g.setColor(Color.BLACK);
		g.fillRect(-40, -5, width + 10, height + 10);
		g.setFont(Game.getGameFont(Font.PLAIN, 12f));
		Color warnColor = (tracked.getHealthPercentage() <= 0.30f ? Color.RED : Color.GREEN);
		if (warnColor.equals(Color.RED)) {
			warnColor = (Game.instance().getTimer() % 50 < 25 ? Color.RED : Color.GREEN);
		}
		ProgressBarRenderer.drawProgressBar(g, cx, cy, width, bh, barDir, Color.DARK_GRAY, warnColor, tracked.getHealthPercentage());
		g.setColor(warnColor);
		TextRenderer.drawString(g, "HLTH", cx - 32, cy - 3);
		cy += bh;

		warnColor = (tracked.getLustPercentage() >= 0.70f ? Color.RED : Color.PINK);
		if (warnColor.equals(Color.RED)) {
			warnColor = (Game.instance().getTimer() % 50 > 25 ? Color.RED : Color.PINK);
		}
		ProgressBarRenderer.drawProgressBar(g, cx, cy, width, bh, barDir, Color.DARK_GRAY, warnColor, tracked.getLustPercentage());
		g.setColor(warnColor);
		TextRenderer.drawString(g, "LUST", cx - 32, cy - 3);
		cy += bh;

		warnColor = (tracked.getFullnessPercentage() >= 0.70f ? Color.RED : Color.ORANGE);
		if (warnColor.equals(Color.RED)) {
			warnColor = (Game.instance().getTimer() % 50 > 25 ? Color.RED : Color.ORANGE);
		}
		ProgressBarRenderer.drawProgressBar(g, cx, cy, width, bh, barDir, Color.DARK_GRAY, warnColor, tracked.getFullnessPercentage());
		g.setColor(warnColor);
		TextRenderer.drawString(g, "FULL", cx - 32, cy - 3);

		g.translate(-tx, -ty);
		g.setComposite(old);
	}

	public void show() {
		if (status == OPEN)
			return;
		status = OPENING;
		timer = Game.instance().getTimer();
	}

	public void showNow() {
		if (status == OPEN)
			return;
		status = OPEN;
		timer = 0;
	}

	public void show(int hideAfter) {
		if (status == OPEN)
			return;
		if (hideAfter < 1)
			return;
		show();
		hideTimer = hideAfter;
	}

	public void showNow(int hideAfter) {
		if (status == OPEN)
			return;
		if (hideAfter < 1)
			return;
		showNow();
		hideTimer = hideAfter;
	}

	public void hide() {
		if (status == CLOSED)
			return;
		status = CLOSING;
		timer = Game.instance().getTimer();
		hideTimer = 0;
	}

	public void hideNow() {
		if (status == CLOSED)
			return;
		status = CLOSED;
		timer = 0;
		hideTimer = 0;
	}

}
