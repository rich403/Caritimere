package cereal.cmere.gfx;

import java.awt.Color;
import java.awt.Graphics2D;

public class ProgressBarRenderer {

	public static final byte DIR_RIGHT = 0x01;
	public static final byte DIR_LEFT = 0x02;
	private static final byte DIR_HORIZ_MASK = 0x0F;
	public static final byte DIR_DOWN = 0x10;
	public static final byte DIR_UP = 0x20;
	private static final byte DIR_VERT_MASK = 0x70;

	public static void drawProgressBar(Graphics2D g, int x, int y, int w, int h, byte dir, Color bg, Color fg, float percentFull) {
		if (percentFull < 0f)
			percentFull = 0f;
		if (percentFull > 1f)
			percentFull = 1f;
		byte horizDir = (byte) (dir & DIR_HORIZ_MASK);
		byte vertDir = (byte) (dir & DIR_VERT_MASK);

		int bx = x;
		int by = y;
		int bw = w;
		int bh = h;

		if (horizDir == DIR_RIGHT) {
			bw = Math.round(w * percentFull);
		} else if (horizDir == DIR_LEFT) {
			bw = Math.round(w * percentFull);
			bx -= (bw - w);
		}

		if (vertDir == DIR_DOWN) {
			bh = Math.round(h * percentFull);
		} else if (horizDir == DIR_UP) {
			bh = Math.round(h * percentFull);
			by -= bh;
		}

		g.setColor(bg);
		g.fillRect(x, y, w, h);

		g.setColor(fg);
		g.fillRect(bx, by, bw, bh);
	}

}
