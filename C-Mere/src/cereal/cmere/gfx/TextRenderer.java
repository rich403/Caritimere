package cereal.cmere.gfx;

import java.awt.Graphics2D;

public class TextRenderer {

	private static final int TAB_WIDTH = 3;

	// TODO
	public static final char TF_RESET = '\u2093'; // Unicode x subscript
	public static final char TF_PRIMARY = '\u2080'; // Unicode 0 subscript
	public static final char TF_SECONDARY = '\u2081'; // Unicode 1 subscript
	public static final char TF_TERTIARY = '\u2082'; // Unicode 2 subscript
	public static final char TF_UNDERLINE = '\u208B'; // Unicode 3 subscript
	public static final char TF_SHAKE = '\u209B'; // Unicode s subscript

	public static void drawString(Graphics2D g, String s, int x, int y) {
		drawString(g, s, x, y, 0);
	}

	public static void drawString(Graphics2D g, String text, int x, int y, int wrapWidth) {
		int tabWidth = getCharWidth(g, ' ') * TAB_WIDTH;
		String[] lines = text.split("\n");
		String[][] words = new String[lines.length][];
		int cx = x;
		int cy = y + getFontHeight(g);
		for (int i = 0; i < lines.length; i++) {
			words[i] = lines[i].split(" ");
		}
		for (int lineIndex = 0; lineIndex < words.length; lineIndex++) {
			for (int wordIndex = 0; wordIndex < words[lineIndex].length; wordIndex++) {
				String word = words[lineIndex][wordIndex];
				if (wrapWidth > 0)
					if (cx + getStringWidth(g, word) >= x + wrapWidth) {
						cx = x;
						cy += getFontHeight(g);
					}
				char[] wordChars = word.toCharArray();
				for (int charIndex = 0; charIndex < wordChars.length; charIndex++) {
					char ch = wordChars[charIndex];
					if (ch == '\t') {
						cx += tabWidth;
						continue;
					}
					g.drawString("" + ch, cx, cy);
					cx += getCharWidth(g, ch);
				}
				cx += getCharWidth(g, ' ');
			}
			cx = x;
			cy += getFontHeight(g);
		}
	}

	public static int getAmtLines(String s) {
		return s.split("\n").length;
	}

	public static int getStringWidth(Graphics2D g, String s) {
		return (int) g.getFontMetrics().getStringBounds(s, g).getWidth();
	}

	public static int getStringHeight(Graphics2D g, String text) {
		return TextRenderer.getStringHeight(g, text, 0);
	}

	public static int getStringHeight(Graphics2D g, String text, int wrapWidth) {
		return getStringLineAmt(g, text, wrapWidth) * getFontHeight(g);
	}

	public static int getStringLineAmt(Graphics2D g, String s, int wrapWidth) {
		int tabWidth = getCharWidth(g, ' ') * TAB_WIDTH;
		String[] lines = s.split("\n");
		String[][] words = new String[lines.length][];
		int cx = 0;
		int amtLines = 1;
		for (int i = 0; i < lines.length; i++) {
			words[i] = lines[i].split(" ");
		}
		for (int lineIndex = 0; lineIndex < words.length; lineIndex++) {
			for (int wordIndex = 0; wordIndex < words[lineIndex].length; wordIndex++) {
				String word = words[lineIndex][wordIndex];
				if (wrapWidth > 0)
					if (cx + getStringWidth(g, word) >= wrapWidth) {
						cx = 0;
						amtLines++;
					}
				char[] wordChars = word.toCharArray();
				for (int charIndex = 0; charIndex < wordChars.length; charIndex++) {
					char ch = wordChars[charIndex];
					if (ch == '\t') {
						cx += tabWidth;
						continue;
					}
					cx += getCharWidth(g, ch);
				}
				cx += getCharWidth(g, ' ');
			}
			cx = 0;
			amtLines++;
		}

		return amtLines;
	}

	public static int getFontHeight(Graphics2D g) {
		return g.getFontMetrics().getHeight() + g.getFontMetrics().getLeading();
	}

	public static int getCharWidth(Graphics2D g, char ch) {
		return g.getFontMetrics().charWidth(ch);
	}
}
