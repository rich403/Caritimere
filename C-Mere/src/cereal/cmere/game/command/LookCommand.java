package cereal.cmere.game.command;

import cereal.cmere.Game;
import cereal.cmere.game.IDescribeable;
import cereal.cmere.game.ITargetable;

public class LookCommand extends GameCommand<LookCommand> {

	public ITargetable target;

	public LookCommand(LookCommandTemplate parent) {
		super(parent, 0, (LookCommand cmd) -> {
			if (cmd.target == null) {
				Game.instance().appendLn("Look target is null!");
				Game.instance().appendLn();
				return;
			} else if (cmd.target instanceof IDescribeable) {
				Game.instance().appendLn(((IDescribeable) cmd.target).getDescription());
				Game.instance().appendLn();
			} else {
				Game.instance().appendLn();
			}
		});
	}

}
