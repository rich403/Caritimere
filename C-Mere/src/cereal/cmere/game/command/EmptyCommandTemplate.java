package cereal.cmere.game.command;

import com.google.common.collect.ImmutableList;

public class EmptyCommandTemplate extends CommandTemplate<EmptyCommand> {

	private final IGameAction<EmptyCommand> action;

	public EmptyCommandTemplate(String name, String desc, ImmutableList<String> aliases, IGameAction<EmptyCommand> action) {
		super(name, desc, aliases, ImmutableList.of());
		this.action = action;
	}

	@Override
	public EmptyCommand parseCommand(String str) throws GameParseException {
		return new EmptyCommand(name, desc, 0, aliases, action);
	}

}
