package cereal.cmere.game.command;

import com.google.common.collect.ImmutableList;

import cereal.cmere.Game;
import cereal.cmere.game.command.args.GCArgDirection;
import cereal.cmere.game.command.args.GCArgDummy;

public class MoveCommandTemplate extends CommandTemplate<MoveCommand> {

	public MoveCommandTemplate() {
		super("move", "Try to move to a new area.", ImmutableList.of("move", "go", "walk", "run"), ImmutableList.of(new GCArgDummy("to"), new GCArgDirection(true)));
	}

	@Override
	public MoveCommand parseCommand(String str) throws GameParseException {
		MoveCommand cmd = new MoveCommand(this);
		String[] args = CommandParser.parseArgs(str);

		if (args.length == 0)
			throw new CommandParseException(cmd, "Expected one argument!");
		else if (args.length < 3)
			if (args.length == 2) {
				cmd.dir = ((GCArgDirection) this.args.get(1)).parse(args[1]);
			} else {
				cmd.dir = ((GCArgDirection) this.args.get(1)).parse(args[0]);
			}

		cmd.length = Math.round(Game.instance().getPlayer().getCurrentArea().getMoveTime(cmd.dir) * Game.instance().getPlayer().getMoveSpeedMultiplier());
		return cmd;
	}

}
