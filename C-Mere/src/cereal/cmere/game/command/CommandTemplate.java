package cereal.cmere.game.command;

import com.google.common.collect.ImmutableList;

import cereal.cmere.Game;
import cereal.cmere.Utils;
import cereal.cmere.game.command.args.CommandArgParseException;
import cereal.cmere.game.command.args.GameCommandArg;
import cereal.cmere.game.command.args.GameCommandArg.Type;

public abstract class CommandTemplate<T extends GameCommand<T>> {

	protected final String name, desc;
	protected final ImmutableList<String> aliases;
	protected final ImmutableList<GameCommandArg<?>> args;
	protected final int argsRequired;

	public CommandTemplate(String name, String desc, ImmutableList<String> aliases, ImmutableList<GameCommandArg<?>> args) {
		this.name = name;
		this.desc = desc;
		this.aliases = Utils.immutableStringListToLowerCase(aliases);
		this.args = args;
		int i = 0;
		for (GameCommandArg<?> arg : this.args) {
			if (arg.required)
				i++;
		}
		this.argsRequired = i;
	}

	public abstract T parseCommand(String str) throws GameParseException;

	public boolean areArgsValid(String[] strArgs) {
		if (strArgs.length < argsRequired)
			return false;
		if (args.size() == 0 && strArgs.length == 0)
			return true;
		int currArg = 0;
		for (GameCommandArg<?> arg : args) {
			try {
				if (currArg < strArgs.length)
					arg.parse(strArgs[currArg++]);
			} catch (CommandArgParseException e) {
				if (!e.getArgument().required) {
					continue;
				}
				Game.instance().appendLn("ERROR:" + e.getMessage());
				Game.instance().appendLn();
				return false;
			}
		}
		return true;
	}

	public String getHelpString() {
		StringBuilder out = new StringBuilder();
		out.append(name);
		out.append(": (");
		out.append(Utils.makeDelimedList(aliases.toArray(new String[aliases.size()]), "/"));
		out.append(")\n");
		out.append("\t");
		out.append(desc);
		out.append("\n\t\tusage: \n\t\t\t");
		out.append(getUsage());
		out.append("\n");
		return out.toString();
	}

	private String getUsage() {
		StringBuilder out = new StringBuilder();
		out.append(aliases.get(0));
		args.forEach((arg) -> {
			out.append(" ");
			if (arg.required)
				out.append("<");
			else
				out.append("[");
			out.append(arg.name);
			if (arg.type != Type.DUMMY) {
				out.append(":");
				out.append(arg.type.name().toLowerCase());
			}
			if (arg.required)
				out.append(">");
			else
				out.append("]");
		});
		return out.toString();
	}

	public int amtOfArgsReqiured() {
		return argsRequired;
	}

}
