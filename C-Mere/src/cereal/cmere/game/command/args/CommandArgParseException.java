package cereal.cmere.game.command.args;

import cereal.cmere.game.command.GameParseException;

public class CommandArgParseException extends GameParseException {
	private static final long serialVersionUID = 1L;

	private final GameCommandArg<?> arg;

	public CommandArgParseException(GameCommandArg<?> arg, String error) {
		super(arg.name + ": " + error);
		this.arg = arg;
	}

	public GameCommandArg<?> getArgument() {
		return arg;
	}

}
