package cereal.cmere.game.command.args;

import java.util.Set;

import cereal.cmere.Game;
import cereal.cmere.game.IInteractable;

public class GCArgInteractable extends GameCommandArg<IInteractable> {

	public GCArgInteractable(boolean required) {
		super(Type.INTERACTABLE, "target", required);
	}

	@Override
	public IInteractable parse(String str) throws CommandArgParseException {
		if (Game.instance().getPlayer().getCurrentArea() == null)
			throw new CommandArgParseException(this, "Player is not in an area!");
		Set<IInteractable> interactables = Game.instance().getPlayer().getCurrentArea().getInteractables();
		for (IInteractable i : interactables) {
			if (i.getAliases().contains(str.toLowerCase())) {
				return i;
			}
		}
		throw new CommandArgParseException(this, "No such interactable: " + str);
	}

}
