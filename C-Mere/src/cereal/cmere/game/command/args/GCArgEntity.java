package cereal.cmere.game.command.args;

import java.util.Set;

import cereal.cmere.Game;
import cereal.cmere.entity.Entity;

public class GCArgEntity extends GameCommandArg<Entity> {

	public GCArgEntity(boolean required) {
		super(Type.ENTITY, "target", required);
	}

	@Override
	public Entity parse(String str) throws CommandArgParseException {
		if (Game.instance().getPlayer().getCurrentArea() == null)
			throw new CommandArgParseException(this, "Player is not in an area!");
		Set<Entity> entities = Game.instance().getPlayer().getCurrentArea().getInteractableEntities();
		for (Entity e : entities) {
			if (e.getAliases().contains(str.toLowerCase())) {
				return e;
			}
		}
		throw new CommandArgParseException(this, "No such entity: " + str);
	}

}
