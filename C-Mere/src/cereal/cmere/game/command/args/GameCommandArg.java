package cereal.cmere.game.command.args;

public abstract class GameCommandArg<T> {

	public enum Type {
		DIRECTION, AREA, NUMBER, STRING, TARGETABLE, ENTITY, INTERACTABLE, DESCRIBEABLE, INVENTORY_ITEM, DUMMY
	}

	public final Type type;
	public final String name;
	public final boolean required;

	protected GameCommandArg(Type type, String name, boolean required) {
		this.type = type;
		this.name = name;
		this.required = required;
	}

	public abstract T parse(String str) throws CommandArgParseException;
}
