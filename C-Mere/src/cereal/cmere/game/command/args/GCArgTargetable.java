package cereal.cmere.game.command.args;

import java.util.Set;

import cereal.cmere.Game;
import cereal.cmere.game.ITargetable;

public class GCArgTargetable extends GameCommandArg<ITargetable> {

	public GCArgTargetable(boolean required) {
		super(Type.TARGETABLE, "target", required);
	}

	@Override
	public ITargetable parse(String str) throws CommandArgParseException {
		if (Game.instance().getPlayer().getCurrentArea() == null)
			throw new CommandArgParseException(this, "Player is not in an area!");
		Set<ITargetable> interactables = Game.instance().getPlayer().getCurrentArea().getTargets();
		for (ITargetable i : interactables) {
			if (i.getAliases().contains(str.toLowerCase())) {
				return i;
			}
		}
		throw new CommandArgParseException(this, "No such interactable: " + str);
	}

}
