package cereal.cmere.game.command;

import com.google.common.collect.ImmutableList;

public class EmptyCommand extends GameCommand<EmptyCommand> {

	public EmptyCommand(String name, String desc, long length, ImmutableList<String> aliases, IGameAction<EmptyCommand> action) {
		super(new CommandTemplate<EmptyCommand>(name, desc, aliases, ImmutableList.of()) {
			@Override
			public EmptyCommand parseCommand(String str) throws GameParseException {
				return new EmptyCommand(name, desc, length, aliases, action);
			}
		}, length, action);
	}
}
