package cereal.cmere.game.command;

public abstract class GameCommand<T extends GameCommand<T>> {

	protected long length;
	protected final IGameAction<T> action;
	protected final CommandTemplate<T> parent;

	public GameCommand(CommandTemplate<T> parent, long length, IGameAction<T> action) {
		this.parent = parent;
		this.length = length;
		this.action = action;
	}

	@SuppressWarnings("unchecked")
	public void doAction() {
		if (action != null)
			action.doAction((T) this);
	}

	public long getLength() {
		return length;
	}

}
