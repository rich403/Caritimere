package cereal.cmere.game.command;

import com.google.common.collect.ImmutableList;

import cereal.cmere.game.command.args.GCArgString;

public class DeleteGameSaveCommandTemplate extends CommandTemplate<DeleteGameSaveCommand> {

	public DeleteGameSaveCommandTemplate() {
		super("delsave", "Deletes a save from the system. If no save is provided it shows a delete menu.", ImmutableList.of("delsave", "delsv", "dsv", "rmsv"), ImmutableList.of(new GCArgString("saveName", false)));
	}

	@Override
	public DeleteGameSaveCommand parseCommand(String str) throws GameParseException {
		DeleteGameSaveCommand cmd = new DeleteGameSaveCommand(this);
		String[] args = CommandParser.parseArgs(str);

		if (args.length > 1)
			throw new CommandParseException(cmd, "Too many arguments!");
		else if (args.length == 1) {
			cmd.profileName = (String) this.args.get(0).parse(args[0]);
		} else {
			cmd.profileName = null;
		}

		return cmd;
	}

}
