package cereal.cmere.game.command;

import com.google.common.collect.ImmutableList;

import cereal.cmere.GameInfo;
import cereal.cmere.game.command.args.GCArgString;

public class SaveGameCommandTemplate extends CommandTemplate<SaveGameCommand> {

	public SaveGameCommandTemplate() {
		super("save", "Saves the game to system, under the given profile name.\n\tIf no name is given, it defaults to 'save'.\n\tSaves are stored under: \n\t\t" + GameInfo.getAppdataFolderPath(), ImmutableList.of("save", "sv"), ImmutableList.of(new GCArgString("saveName", false)));
	}

	@Override
	public SaveGameCommand parseCommand(String str) throws GameParseException {
		SaveGameCommand cmd = new SaveGameCommand(this);
		String[] args = CommandParser.parseArgs(str);

		if (args.length > 1)
			throw new CommandParseException(cmd, "Too many arguments!");
		else if (args.length == 1) {
			cmd.profileName = (String) this.args.get(0).parse(args[0]);
		} else {
			cmd.profileName = "save";
		}

		return cmd;
	}

}
