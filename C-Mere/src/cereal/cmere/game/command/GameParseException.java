package cereal.cmere.game.command;

public class GameParseException extends Exception {
	private static final long serialVersionUID = 1L;

	protected final String msg;

	public GameParseException(String msg) {
		this.msg = msg;
	}

	@Override
	public String getMessage() {
		return msg;
	}

}
