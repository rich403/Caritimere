package cereal.cmere.game.command;

import cereal.cmere.Game;

public class SaveGameCommand extends GameCommand<SaveGameCommand> {

	public String profileName;

	public SaveGameCommand(CommandTemplate<SaveGameCommand> parent) {
		super(parent, 0, (SaveGameCommand cmd) -> {
			Game.instance().saveGame(cmd.profileName);
		});
	}

}
