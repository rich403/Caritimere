package cereal.cmere.game.command;

import com.google.common.collect.ImmutableList;

import cereal.cmere.GameInfo;
import cereal.cmere.game.command.args.GCArgString;

public class LoadGameCommandTemplate extends CommandTemplate<LoadGameCommand> {

	public LoadGameCommandTemplate() {
		super("load", "Loads the game from system, under the given profile name.\n\tIf no name is given, it shows a save selection menu.\n\tSaves are stored under: \n\t\t" + GameInfo.getAppdataFolderPath(), ImmutableList.of("load", "ld"), ImmutableList.of(new GCArgString("saveName", false)));
	}

	@Override
	public LoadGameCommand parseCommand(String str) throws GameParseException {
		LoadGameCommand cmd = new LoadGameCommand(this);
		String[] args = CommandParser.parseArgs(str);

		if (args.length > 1)
			throw new CommandParseException(cmd, "Too many arguments!");
		else if (args.length == 1) {
			cmd.profileName = (String) this.args.get(0).parse(args[0]);
		} else {
			cmd.profileName = null;
		}

		return cmd;
	}

}
