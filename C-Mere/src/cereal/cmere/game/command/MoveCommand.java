package cereal.cmere.game.command;

import cereal.cmere.Game;
import cereal.cmere.map.Direction;

public class MoveCommand extends GameCommand<MoveCommand> {

	public Direction dir;

	public MoveCommand(MoveCommandTemplate parent) {
		super(parent, 0, (MoveCommand cmd) -> {
			if (Game.instance().getPlayer().move(cmd.dir)) {
				Game.instance().passTime(Game.instance().getPlayer().getGetMoveLength());
				Game.instance().getPlayer().getCurrentArea().onEntry();
			} else {
				Game.instance().appendLn("You can't move to the " + cmd.dir.name().toLowerCase() + ".");
				Game.instance().appendLn();
			}
		});
	}

	@Override
	public long getLength() {
		return length;
	}

}
