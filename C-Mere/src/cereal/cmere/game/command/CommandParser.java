package cereal.cmere.game.command;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import cereal.cmere.Game;
import cereal.cmere.GameInfo;
import cereal.cmere.Utils;
import cereal.cmere.game.Pair;

public class CommandParser {

	// The generics in here are a complete shitshow.

	private static Set<CommandTemplate<? extends GameCommand<?>>> commandTemplates = new HashSet<CommandTemplate<? extends GameCommand<?>>>();

	public static final String parseName(String str) {
		return str.trim().split(" ")[0].toLowerCase();
	}

	public static final String[] parseArgs(String str) {
		String[] split = str.trim().split(" ");
		if (split.length > 1) {
			String[] out = new String[split.length - 1];
			for (int i = 1; i < split.length; i++) {
				out[i - 1] = split[i];
			}
			return out;
		}
		return new String[0];
	}

	public static GameCommand<? extends GameCommand<?>> parseCommand(String str) throws GameParseException {
		String name = parseName(str);
		String[] args = parseArgs(str);
		for (CommandTemplate<? extends GameCommand<?>> template : commandTemplates) {
			if (template.aliases.contains(name) && template.areArgsValid(args)) {
				return template.parseCommand(str);
			}
		}
		throw new GameParseException("No such command: " + name);
	}

	public static Set<CommandTemplate<? extends GameCommand<?>>> getCommandsThatStartWith(String str) {
		Set<CommandTemplate<? extends GameCommand<?>>> matchingCommands = new HashSet<CommandTemplate<? extends GameCommand<?>>>();
		for (CommandTemplate<? extends GameCommand<? extends GameCommand<?>>> cmd : commandTemplates) {
			for (String alias : cmd.aliases) {
				if (alias.startsWith(str.toLowerCase())) {
					matchingCommands.add(cmd);
					break;
				}
			}
		}
		return matchingCommands;
	}

	public static Map<Long, Pair<String, CommandTemplate<? extends GameCommand<?>>>> getCommandsThatAreClosestTo(String str, long fuzzyness) {
		Map<Long, Pair<String, CommandTemplate<? extends GameCommand<?>>>> closeCommands = new HashMap<>();
		for (CommandTemplate<? extends GameCommand<? extends GameCommand<?>>> cmd : commandTemplates) {
			long levDist = Long.MAX_VALUE;
			String bestStr = "";
			for (String alias : cmd.aliases) {
				long aliasDist = Utils.levenstheinDistance(str.toLowerCase(), alias);
				if (str.toLowerCase().startsWith(alias.substring(0, 1)))
					if (aliasDist < levDist) {
						levDist = aliasDist;
						bestStr = alias;
					}
			}
			if (levDist <= fuzzyness) {
				closeCommands.put(levDist, new Pair<String, CommandTemplate<? extends GameCommand<?>>>(bestStr, cmd));
			}
		}
		return closeCommands;
	}

	public static void initalize() {
		commandTemplates.clear();
		commandTemplates.add(new EmptyCommandTemplate("help", "Display this text.", ImmutableList.of("help", "?", "h"), (EmptyCommand cmd) -> {
			Game.instance().appendLn("\t\tCommand Help");
			Game.instance().appendLn("=========================");
			for (CommandTemplate<? extends GameCommand<?>> cmdTmp : commandTemplates) {
				Game.instance().appendLn();
				Game.instance().appendLn(cmdTmp.getHelpString());
			}
		}));
		commandTemplates.add(new EmptyCommandTemplate("autoscroll", "Toggle autoscroll.", ImmutableList.of("autoscroll", "as"), (EmptyCommand cmd) -> {
			Game.instance().appendLn("Auto scroll is " + (Game.instance().toggleAutoScroll() ? "enabled" : "disabled") + ".");
			Game.instance().appendLn();
		}));
		commandTemplates.add(new EmptyCommandTemplate("version", "Shows the version of the game and what's new.", ImmutableList.of("version", "whatsnew"), (EmptyCommand cmd) -> {
			Game.instance().appendLn(GameInfo.NAME + " V" + GameInfo.VERSION);
			Game.instance().appendLn(GameInfo.WHATSNEW);
			Game.instance().appendLn();
			Game.instance().appendLn();
		}));
		commandTemplates.add(new LookCommandTemplate());
		commandTemplates.add(new MoveCommandTemplate());
		commandTemplates.add(new SaveGameCommandTemplate());
		commandTemplates.add(new LoadGameCommandTemplate());
		commandTemplates.add(new DeleteGameSaveCommandTemplate());
	}

}
