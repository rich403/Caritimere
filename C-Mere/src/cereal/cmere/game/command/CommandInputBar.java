package cereal.cmere.game.command;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import cereal.cmere.Game;
import cereal.cmere.GameInfo;
import cereal.cmere.Utils;
import cereal.cmere.game.Pair;
import cereal.cmere.gfx.TextRenderer;

public class CommandInputBar {

	public static final int BAR_HEIGHT = 20;
	public static final int[] BAR_MARGIN = new int[] { 3, 5 }; // Top, left
	public static final String ALLOWED_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_=+[]{}|;:\"',<>./?!@#$%^&*()`~/ ";
	public static final int MAX_INPUT_LENGTH = 46, HISTORY_SIZE = 10, COMMAND_SUGGESTION_FUZZYNESS = 3;

	private Stack<String> history = new Stack<String>();
	private int histIndex = 0;

	private Map<Long, Pair<String, CommandTemplate<? extends GameCommand<?>>>> suggestions = new HashMap<>();

	private String entry = "";

	public void onKeyTyped(KeyEvent e) {
		if (e.getKeyChar() == '\n') {
			if (!entry.isEmpty()) {
				if (!history.empty()) {
					history.push(entry);
				} else {
					history.push(entry);
					if (history.size() > HISTORY_SIZE)
						history.pop();
				}

				Game.instance().onCommand(entry);
				entry = "";
				histIndex = 0;
				suggestions.clear();
				return;
			}
		}

		if (e.getKeyChar() == '\b') {
			if (entry.length() > 0) {
				entry = entry.substring(0, entry.length() - 1);
				histIndex = 0;
				if (entry.length() == 0)
					suggestions.clear();
				else if (CommandParser.parseArgs(entry).length == 0)
					suggestions = CommandParser.getCommandsThatAreClosestTo(entry, COMMAND_SUGGESTION_FUZZYNESS);
			}
			return;
		}
		if (entry.length() == MAX_INPUT_LENGTH)
			return;

		if (ALLOWED_CHARS.contains("" + e.getKeyChar()))
			entry += e.getKeyChar();

		if (CommandParser.parseArgs(entry).length == 0)
			suggestions = CommandParser.getCommandsThatAreClosestTo(entry, COMMAND_SUGGESTION_FUZZYNESS);
	}

	public void onKeyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_TAB) {
			System.out.println("TAB");
			if (!suggestions.isEmpty()) {
				Pair<String, CommandTemplate<? extends GameCommand<?>>> chosen = Utils.getLowestMappedValue(suggestions);
				entry = chosen.getFirst();
			}
		}
		if (history.isEmpty())
			return;
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			entry = history.get(history.size() - histIndex - 1);
			histIndex++;
			if (histIndex >= history.size())
				histIndex--;
			suggestions.clear();
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			histIndex--;
			if (histIndex < 0)
				histIndex++;
			entry = history.get(history.size() - histIndex - 1);
			suggestions.clear();
		}
	}

	public void draw(Graphics2D g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, GameInfo.HEIGHT - BAR_HEIGHT, GameInfo.WIDTH, BAR_HEIGHT);
		g.setColor(Color.WHITE);
		g.drawRect(0, GameInfo.HEIGHT - BAR_HEIGHT, GameInfo.WIDTH - 1, BAR_HEIGHT - 1);
		g.setFont(Game.getGameFont());
		if (entry.length() == MAX_INPUT_LENGTH)
			g.setColor(Color.YELLOW);
		int lx = BAR_MARGIN[1];
		int ly = GameInfo.HEIGHT - BAR_HEIGHT + BAR_MARGIN[0];
		TextRenderer.drawString(g, GameInfo.HANDLE, lx, ly);
		lx += TextRenderer.getStringWidth(g, GameInfo.HANDLE);
		TextRenderer.drawString(g, entry, lx, ly);
		if (suggestions.size() > 0 && !Game.instance().isAwaitingTextInput()) {
			Set<Integer> widthsSet = new HashSet<>();
			suggestions.forEach((Long closeness, Pair<String, CommandTemplate<? extends GameCommand<?>>> pair) -> {
				widthsSet.add(TextRenderer.getStringWidth(g, pair.getFirst()));
			});
			int boxWidth = Utils.getMaxNumber(widthsSet.toArray(new Integer[widthsSet.size()])) + 10;
			int boxHeight = TextRenderer.getFontHeight(g) * suggestions.size() + 10;
			g.setColor(Color.BLACK);
			g.fillRect(0, GameInfo.HEIGHT - BAR_HEIGHT - boxHeight, boxWidth, boxHeight);
			g.setColor(Color.WHITE);
			int y = GameInfo.HEIGHT - BAR_HEIGHT - boxHeight;
			g.drawRect(0, y, boxWidth - 1, boxHeight - 1);
			y += 5;
			for (Entry<Long, Pair<String, CommandTemplate<? extends GameCommand<?>>>> e : suggestions.entrySet()) {
				TextRenderer.drawString(g, e.getValue().getFirst(), 5, y);
				y += TextRenderer.getFontHeight(g);
			}
		}
		if (Game.instance().getTimer() % 500 > 250)
			return;
		if (entry.length() == MAX_INPUT_LENGTH)
			g.setColor(Color.YELLOW);
		lx += TextRenderer.getStringWidth(g, entry);
		TextRenderer.drawString(g, GameInfo.CARAT, lx, ly);
	}

}
