package cereal.cmere.game;

public interface IDescribeable {

	public String getName();

	public String getDescription();

}
