package cereal.cmere.game;

public interface IStatistical {

	public int getLevel();

	public int getMaxLevel();

	public int getXp();

	public int getMaxXp();

	public void setXp(int xp);

	public void addXp(int amt);

	/**
	 * Intelligence stat.
	 * 
	 * @return Intelligence.
	 */
	public int getSagacity();

	public void setSagacity(int sag);

	/**
	 * Strength stat.
	 * 
	 * @return Strength.
	 */
	public int getTenacity();

	public void setTenacity(int ten);

	/**
	 * Seductiveness stat.
	 * 
	 * @return Seductiveness.
	 */
	public int getVivacity();

	public void setVivacity(int viv);

	public int getMaxHealth();

	public int getHealth();

	public default float getHealthPercentage() {
		return (float) getHealth() / (float) getMaxHealth();
	}

	public int setHealth(int health);

	public int addHealth(int amt);

	public int getMaxLust();

	public int getLust();

	public default float getLustPercentage() {
		return (float) getLust() / (float) getMaxLust();
	}

	public int setLust(int lust);

	public int addLust(int amt);

	public int getMaxFullness();

	public int getFullness();

	public default float getFullnessPercentage() {
		return (float) getFullness() / (float) getMaxFullness();
	}

	public int setFullness(int fullness);

	public int addFullness(int amt);
}
