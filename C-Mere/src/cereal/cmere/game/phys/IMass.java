package cereal.cmere.game.phys;

/**
 * Interface that allows something to have a weight and a volume.
 * 
 * @author richi
 *
 */
public interface IMass {

	/**
	 * @return The volume of this {@link IMass}, in m�
	 */
	public double getVolume();

	/**
	 * @return The weight of this {@link IMass}, in kg.
	 */
	public double getWeight();

	/**
	 * @return Gets the average density of the {@link IMass}, in kg/m�
	 */
	public default double getAverageDensity() {
		return getWeight() / getVolume();
	}

}
