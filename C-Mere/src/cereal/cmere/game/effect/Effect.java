package cereal.cmere.game.effect;

import cereal.cmere.Game;
import cereal.cmere.game.IDFactory;
import cereal.cmere.game.IDescribeable;
import cereal.cmere.io.ISSerializable;
import net.keabotstudios.superserial.containers.SSObject;

public abstract class Effect implements IDescribeable, ISSerializable {

	public final Runnable effect;
	public final String name, description, effectText;

	public Effect(String name, String description, String effectText, Runnable effect) {
		this.description = description;
		this.name = name;
		this.effect = effect;
		this.effectText = effectText;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void doEffect() {
		effect.run();
		Game.instance().appendLn(effectText);
		Game.instance().appendLn();
	}

	@Override
	public SSObject serialize() {
		SSObject obj = new SSObject(getSerialName());
		return obj;
	}

	@Override
	public String getSerialName() {
		return "effect:" + IDFactory.generateID();
	}

}
