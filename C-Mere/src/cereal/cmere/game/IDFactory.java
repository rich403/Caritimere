package cereal.cmere.game;

import java.util.HashSet;
import java.util.Set;

public class IDFactory {

	private static final String ALPHANUMERICS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private static final int ID_LENGTH = 6;

	private static final Set<String> IDs = new HashSet<String>();

	public static String generateID() {
		StringBuilder builder = new StringBuilder();

		int count = ID_LENGTH;
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHANUMERICS.length());
			builder.append(ALPHANUMERICS.charAt(character));
		}
		if (!IDs.contains(builder.toString())) {
			useID(builder.toString());
			return builder.toString();
		} else {
			return generateID();
		}
	}

	public static boolean freeID(String id) {
		if (!IDs.contains(id))
			return false;
		return IDs.remove(id);
	}

	public static boolean useID(String id) {
		if (IDs.contains(id))
			return false;
		return IDs.add(id);
	}

	public static int getAmtIDs() {
		return IDs.size();
	}

}
