package cereal.cmere.map;

import cereal.cmere.io.ISSerializable;
import net.keabotstudios.superserial.containers.SSField;
import net.keabotstudios.superserial.containers.SSObject;

public class MapPos implements ILocatable, Cloneable, ISSerializable {

	private static final MapPos NULL = new MapPos();

	private int x, y, z;

	private MapPos() {
	};

	public MapPos(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void move(int x, int y, int z) {
		this.x += x;
		this.y += y;
		this.z += z;
	}

	public MapPos add(int x, int y, int z) {
		MapPos result = this.clone();
		result.move(x, y, z);
		return result;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	@Override
	public MapPos getPos() {
		return this;
	}

	@Override
	protected MapPos clone() {
		return new MapPos(x, y, z);
	}

	public void setPos(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public static MapPos EMPTY() {
		return NULL.clone();
	}

	@Override
	public boolean setPos(MapPos pos) {
		this.x = pos.x;
		this.y = pos.y;
		this.z = pos.z;
		return true;
	}

	public boolean equals(MapPos pos) {
		return equals((ILocatable) pos);
	}

	@Override
	public String toString() {
		return "pos[" + x + "," + y + "," + z + "]";
	}

	@Override
	public SSObject serialize() {
		SSObject obj = new SSObject(getSerialName());
		obj.addField(SSField.Integer("x", x));
		obj.addField(SSField.Integer("y", y));
		obj.addField(SSField.Integer("z", z));
		return obj;
	}

	@Override
	public void deserialize(SSObject obj) {
		x = obj.getField("x").getInteger();
		y = obj.getField("y").getInteger();
		z = obj.getField("z").getInteger();
	}

	@Override
	public String getSerialName() {
		return "pos";
	}
}
