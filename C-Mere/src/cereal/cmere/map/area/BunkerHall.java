package cereal.cmere.map.area;

import java.awt.Graphics2D;

import cereal.cmere.Game;
import cereal.cmere.game.command.GameCommand;
import cereal.cmere.map.Direction;
import net.keabotstudios.superserial.containers.SSField;
import net.keabotstudios.superserial.containers.SSObject;

public class BunkerHall extends Area {

	public enum Type {
		E_W, // < >
		N_S, // ^ v

		N_E, // ^ >
		N_W, // < ^
		S_W, // < v
		S_E, // v >

		N_S_E_W, // < ^ v >

		N_S_E, // ^ v >
		N_S_W, // < ^ v
		W_S_E, // < v >
		N_E_W // < ^ >
	}

	private Type type;

	public BunkerHall() {
		this(null, 0, 0);
	}

	public BunkerHall(Type type, int miniMapX, int miniMapY) {
		super("Bunker Hall", miniMapX, miniMapY);
		this.type = type;

		updateType();
	}

	private void updateType() {
		if (this.type == null)
			return;

		switch (this.type) {
		case E_W:
			setSideOpen(Direction.EAST, true);
			setSideOpen(Direction.WEST, true);
			break;
		case N_S:
			setSideOpen(Direction.NORTH, true);
			setSideOpen(Direction.SOUTH, true);
			break;
		case S_W:
			setSideOpen(Direction.SOUTH, true);
			setSideOpen(Direction.WEST, true);
			break;
		case S_E:
			setSideOpen(Direction.SOUTH, true);
			setSideOpen(Direction.EAST, true);
			break;
		case N_E:
			setSideOpen(Direction.NORTH, true);
			setSideOpen(Direction.EAST, true);
			break;
		case N_W:
			setSideOpen(Direction.NORTH, true);
			setSideOpen(Direction.WEST, true);
			break;
		case N_S_E_W:
			setSideOpen(Direction.NORTH, true);
			setSideOpen(Direction.SOUTH, true);
			setSideOpen(Direction.EAST, true);
			setSideOpen(Direction.WEST, true);
			break;
		case N_S_E:
			setSideOpen(Direction.NORTH, true);
			setSideOpen(Direction.SOUTH, true);
			setSideOpen(Direction.EAST, true);
			break;
		case N_S_W:
			setSideOpen(Direction.NORTH, true);
			setSideOpen(Direction.SOUTH, true);
			setSideOpen(Direction.WEST, true);
			break;
		case W_S_E:
			setSideOpen(Direction.WEST, true);
			setSideOpen(Direction.SOUTH, true);
			setSideOpen(Direction.WEST, true);
			break;
		case N_E_W:
			setSideOpen(Direction.NORTH, true);
			setSideOpen(Direction.EAST, true);
			setSideOpen(Direction.WEST, true);
			break;
		}
	}

	@Override
	public void onInteracted(GameCommand<? extends GameCommand<?>> cmd) {

	}

	@Override
	public void onEntry() {
		super.onEntry();
		Game.instance().getPlayer().addHealth(-1);
		Game.instance().getPlayer().addLust(1);
		Game.instance().appendLn("You are coughing and your lungs burn, but the gas also seems to be arousing you.");
	}

	@Override
	public String getAreaText() {
		return "The dark hallway has obviously never seen light in decades. The ceiling creaks above you. There are pipes running along the ceiling here. You can hear hissing from some of them, and it's getting hard to breathe.";
	}

	@Override
	public void drawOnMiniMap(Graphics2D g) {
		// Polygon p = new Polygon();
		// int width = 60;
		// int height = 40;
		// switch(type) {
		// case E_W:
		// p.addPoint(-height / 4, -width / 2);
		// p.addPoint(-height / 4, width / 2);
		// p.addPoint(height / 4, width / 2);
		// p.addPoint(height / 4, width / 2);
		// break;
		// case N_E:
		// p.addPoint(-height / 2, -width / 4);
		// p.addPoint(-height / 2, width / 4);
		// p.addPoint(height / 2, width / 4);
		// p.addPoint(height / 2, width / 4);
		// break;
		// case N_E_W:
		// p.addPoint(-height / 2, -width / 4);
		// p.addPoint(-height / 2, width / 4);
		// p.addPoint(height / 2, width / 4);
		// p.addPoint(height / 2, width / 4);
		// break;
		// case N_S:
		// break;
		// case N_S_E:
		// break;
		// case N_S_E_W:
		// break;
		// case N_S_W:
		// break;
		// case N_W:
		// break;
		// case S_E:
		// break;
		// case S_W:
		// break;
		// case W_S_E:
		// break;
		// }
		// g.setColor(Color.BLACK);
		// g.fill(p);
		// g.setColor(Color.WHITE);
		// g.draw(p);

		// TODO all of this
		super.drawOnMiniMap(g);
	}

	@Override
	protected void serializeArea(SSObject obj) {
		obj.addField(SSField.Byte("type", (byte) type.ordinal()));
	}

	@Override
	protected void deserializeArea(SSObject obj) {
		type = Type.values()[obj.getField("type").getByte()];
		updateType();
	}

}
