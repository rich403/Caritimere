package cereal.cmere.map.area;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import cereal.cmere.Game;
import cereal.cmere.Utils;
import cereal.cmere.entity.Entity;
import cereal.cmere.game.IDFactory;
import cereal.cmere.game.IDescribeable;
import cereal.cmere.game.IInteractable;
import cereal.cmere.game.ITargetable;
import cereal.cmere.game.effect.Effect;
import cereal.cmere.io.ISSerializable;
import cereal.cmere.map.Direction;
import cereal.cmere.map.ILocatable;
import cereal.cmere.map.Map;
import cereal.cmere.map.MapPos;
import net.keabotstudios.superserial.containers.SSField;
import net.keabotstudios.superserial.containers.SSObject;
import net.keabotstudios.superserial.containers.SSString;

public abstract class Area implements ILocatable, IDescribeable, IInteractable, ISSerializable {

	protected Map parent;
	protected String areaName;
	protected MapPos pos;
	protected final Set<Direction> exits = new HashSet<Direction>();
	protected boolean[] sidesOpen;
	protected Set<Entity> entities;
	protected Set<Effect> effects;
	protected int miniMapX;
	protected int miniMapY;

	public Area(String name, int miniMapX, int miniMapY) {
		this.areaName = name;
		this.sidesOpen = new boolean[Direction.values().length];
		calculateExits();
		this.entities = new HashSet<>();
		this.miniMapX = miniMapX;
		this.miniMapY = miniMapY;
	}

	public void calculateExits() {
		exits.clear();
		for (Direction d : Direction.values())
			if (canMoveTo(d))
				exits.add(d);
	}

	public void onEntry() {
		Game.instance().appendLn(getDescription());
	}

	public void onExit() {

	}

	private String getLookText() {
		StringBuilder out = new StringBuilder();
		out.append("==========\n");
		out.append(areaName);
		out.append("\n==========\n");
		out.append(getAreaText());
		return out.toString();
	}

	@Override
	public String getName() {
		return areaName;
	}

	public abstract String getAreaText();

	@Override
	public String getDescription() {
		return getLookText() + "\n" + listEntities() + "\n" + listExits();
	}

	private String listEntities() {
		StringBuilder out = new StringBuilder();
		for (Entity e : entities) {
			out.append("There is " + Utils.article(e.getName()) + " " + e.getName() + " here.\n");
		}
		return out.toString();
	}

	private String listExits() {
		StringBuilder out = new StringBuilder();
		for (Direction d : exits) {
			out.append("There is an exit " + d.getName() + " from here.\n");
		}
		return out.toString();
	}

	@Override
	public MapPos getPos() {
		return pos;
	}

	@Override
	public boolean setPos(MapPos pos) {
		this.pos = pos;
		return true;
	}

	public Set<Direction> getExits() {
		return exits;
	}

	public boolean isSideOpen(Direction d) {
		return this.sidesOpen[d.ordinal()];
	}

	public void setSideOpen(Direction d, boolean open) {
		this.sidesOpen[d.ordinal()] = open;
	}

	public boolean canMoveTo(Direction d) {
		if (parent == null)
			return false;
		if (isSideOpen(d)) {
			Area a = parent.getAreaAt(d.from(this));
			if (a == null)
				return false;
			return a.isSideOpen(d.opposite());
		}
		return false;
	}

	public Set<Entity> getInteractableEntities() {
		Set<Entity> out = getVisibleEntities();
		entities.forEach((e) -> {
			if (e.isInteractable())
				out.add(e);
		});
		return out;
	}

	public Set<Entity> getVisibleEntities() {
		Set<Entity> out = new HashSet<Entity>();
		entities.forEach((e) -> {
			if (!e.isHidden())
				out.add(e);
		});
		return out;
	}

	public Set<Entity> getEntities() {
		return entities;
	}

	public Set<IInteractable> getInteractables() {
		Set<IInteractable> out = new HashSet<IInteractable>();
		out.add(this);
		out.addAll(getInteractableEntities());
		if (this.getPos().equals(Game.instance().getPlayer()))
			out.add(Game.instance().getPlayer());
		return out;
	}

	public Set<IDescribeable> getDescribeables() {
		Set<IDescribeable> out = new HashSet<IDescribeable>();
		out.add(this);
		out.addAll(getInteractableEntities());
		if (this.getPos().equals(Game.instance().getPlayer()))
			out.add(Game.instance().getPlayer());
		return out;
	}

	@Override
	public boolean isHidden() {
		return false;
	}

	@Override
	public boolean isInteractable() {
		return true;
	}

	@Override
	public ImmutableList<String> getAliases() {
		return ImmutableList.of("here", "area", "around");
	}

	public void setParent(Map map) {
		this.parent = map;
	}

	@Override
	public String toString() {
		return getName() + "(" + pos.getX() + ", " + pos.getY() + ", " + pos.getZ() + ")";
	}

	public Set<ITargetable> getTargets() {
		Set<ITargetable> out = new HashSet<ITargetable>();
		out.add(this);
		out.addAll(getInteractableEntities());
		if (this.getPos().equals(Game.instance().getPlayer()))
			out.add(Game.instance().getPlayer());
		return out;
	}

	public long getMoveTime(Direction dir) {
		return 0;
	}

	public void drawOnMiniMap(Graphics2D g) {
		g.setColor(Color.BLACK);
		g.fillRect(-15, -10, 30, 20);
		g.setColor(Color.WHITE);
		g.drawRect(-15, -10, 29, 19);
	}

	public Dimension getMinimapSize() {
		return new Dimension(30, 20);
	}

	public int getMiniMapX() {
		return miniMapX;
	}

	public int getMiniMapY() {
		return miniMapY;
	}

	public Class<? extends Area> getAreaClass() {
		return this.getClass();
	}

	@Override
	public final SSObject serialize() {
		SSObject obj = new SSObject(getSerialName());
		obj.addString(new SSString("class", getAreaClass().getName()));
		obj.addString(new SSString("name", getName()));
		obj.addObject(pos.serialize());
		obj.addField(SSField.Integer("mmX", miniMapX));
		obj.addField(SSField.Integer("mmY", miniMapY));
		SSObject ents = new SSObject("entities");
		for (Entity e : entities)
			ents.addObject(e.serialize());
		obj.addObject(ents);
		serializeArea(obj);
		return obj;
	}

	public final void deserialize(SSObject data) {
		if (!data.getString("class").getString().equals(getAreaClass().getName()))
			throw new ClassFormatError("Area class mismatch! \"" + data.getString("class") + "\" =/= \"" + getAreaClass().getName() + "\"");
		this.areaName = data.getString("name").getString();
		this.pos = MapPos.EMPTY();
		this.pos.deserialize(data.getObject("pos"));
		this.miniMapX = data.getField("mmX").getInteger();
		this.miniMapY = data.getField("mmY").getInteger();
		this.entities.clear();
		SSObject ents = data.getObject("entities");
		for (SSObject obj : ents.getObjects()) {
			try {
				Entity e = (Entity) Class.forName(obj.getString("class").getString()).newInstance();
				e.deserialize(obj);
				this.entities.add(e);
			} catch (Exception e1) {
				System.err.println("Can't deserialize area: " + obj.getString("class").getString());
			}
		}
		deserializeArea(data);
	}

	protected abstract void serializeArea(SSObject obj);

	protected abstract void deserializeArea(SSObject data);

	@Override
	public final String getSerialName() {
		return "area:" + IDFactory.generateID();
	}

}
