package cereal.cmere.map;

import java.util.HashSet;
import java.util.Set;

import cereal.cmere.Game;
import cereal.cmere.entity.Entity;
import cereal.cmere.io.ISSerializable;
import cereal.cmere.map.area.Area;
import net.keabotstudios.superserial.containers.SSObject;

public class Map implements ISSerializable {

	private Set<Area> areas;

	public Map() {
		areas = new HashSet<>();
	}

	public Area getAreaAt(ILocatable pos) {
		for (Area a : areas)
			if (a.getPos().equals(pos))
				return a;
		return null;
	}

	public void addArea(Area a, MapPos pos) {
		if (getAreaAt(pos) != null)
			return;
		a.setPos(pos);
		a.setParent(this);
		areas.add(a);
		for (Area ar : areas)
			ar.calculateExits();
	}

	public void addArea(Area a, int x, int y, int z) {
		this.addArea(a, new MapPos(x, y, z));
	}

	public void removeArea(MapPos pos) {
		Area a = getAreaAt(pos);
		if (a != null)
			areas.remove(a);
	}

	public void removeArea(int x, int y, int z) {
		this.removeArea(new MapPos(x, y, z));
	}

	public boolean equals(Map map) {
		return areas.equals(map.areas);
	}

	/**
	 * @return All entities in all the areas of this map, including the player.
	 */
	public Set<Entity> getAllEntites() {
		Set<Entity> out = new HashSet<Entity>();
		areas.forEach((area) -> {
			out.addAll(area.getEntities());
		});
		out.add(Game.instance().getPlayer());
		return out;
	}

	public Set<Area> getAreas() {
		return areas;
	}

	public boolean isPosWithin(ILocatable pos) {
		return getAreaAt(pos) != null;
	}

	@Override
	public SSObject serialize() {
		SSObject obj = new SSObject(getSerialName());
		SSObject areas = new SSObject("areas");
		this.areas.forEach((area) -> {
			areas.addObject(area.serialize());
		});
		obj.addObject(areas);
		return obj;
	}

	@Override
	public void deserialize(SSObject data) {
		this.areas.clear();
		SSObject areas = data.getObject("areas");
		for (SSObject obj : areas.getObjects()) {
			try {
				Area a = (Area) Class.forName(obj.getString("class").getString()).newInstance();
				a.deserialize(obj);
				addArea(a, a.getPos());
			} catch (Exception e1) {
				e1.printStackTrace();
				System.err.println("Can't deserialize area: " + obj.getString("class").getString());
			}
		}
	}

	@Override
	public String getSerialName() {
		return "map";
	}

}
