package cereal.cmere.map;

import cereal.cmere.map.area.BunkerHall;
import cereal.cmere.map.area.BunkerHall.Type;
import cereal.cmere.map.area.LabArea;

public class LabMap extends Map {

	public LabMap() {
		LabArea lab = new LabArea(0, 0);
		lab.setSideOpen(Direction.NORTH, true);
		addArea(lab, 0, 0, 0);
		addArea(new BunkerHall(Type.N_S, 0, 30), 0, 1, 0);
		addArea(new BunkerHall(Type.N_S_E, 0, 60), 0, 2, 0);
		addArea(new BunkerHall(Type.E_W, 50, 60), 1, 2, 0);
	}

}
