package cereal.cmere.map;

public interface ILocatable {

	public MapPos getPos();

	public boolean setPos(MapPos pos);

	public default boolean equals(ILocatable loc) {
		return this.getPos().getX() == loc.getPos().getX() && this.getPos().getY() == loc.getPos().getY() && this.getPos().getZ() == loc.getPos().getZ();
	}

}
